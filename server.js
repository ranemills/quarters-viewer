var express = require('express');
var namespace = require('express-namespace');
var _ = require('lodash');
var mockServer = express();

mockServer.use('/scripts', express.static('scripts'));
mockServer.use('/bower_components', express.static('bower_components'));
mockServer.use('/stylesheets', express.static('stylesheets'));

function generatePropertyCountList() {
  var letters = ['a', 'b', 'c', 'd', 'e', 'f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
  var list = [];
  for(var i = 0; i < Math.ceil(Math.random()*200); i++) {
    var name = '';
    for(var j = 0; j<10; j++) {
      name = name + letters[Math.floor(Math.random()*26)];
    }
    list.push({property: name, count: Math.ceil(Math.random()*100)})
  }
  return _.reverse(_.sortBy(list, 'count'));
}


mockServer.get('/', function(req, res) {
    res.sendFile('index.html', {root: __dirname });
});

mockServer.get('/views/:name', function(req, res) {
    res.sendFile('views/' + req.params.name, {root: __dirname });
});

mockServer.get('/views/directives/:name', function(req, res) {
    res.sendFile('views/directives/' + req.params.name, {root: __dirname });
});

mockServer.get('/app.js', function(req, res) {
    res.sendFile('app.js', {root: __dirname });
});

mockServer.namespace('/api', function() {

  mockServer.get('/import', function() { return {}; });
  mockServer.get('/auth/register', function() { return {}; });
  mockServer.get('/auth/user', function(req, res) { res.send({name: 'mockName', hasImported: true}); });

  mockServer.namespace('/stats', function() {
    mockServer.get('/available', function(req, res) { res.send(['methods', 'stages']); });
    mockServer.get('/filters', function(req, res) {

      var methods = generatePropertyCountList();
      var stages = generatePropertyCountList();
      var dates = generatePropertyCountList();
      var ringers = generatePropertyCountList();
      res.send({
        methods: methods,
        stages: stages,
        dates: dates,
        ringers: ringers
      });
    });
  });

  mockServer.get('/performances/list', function(req, res) {
    res.send([]);
  });

});

// listen (start app with node server.js) ======================================
mockServer.listen(8011);
console.log("App listening on port 8011");
